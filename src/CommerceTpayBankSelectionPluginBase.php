<?php

namespace Drupal\commerce_tpay;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Plugin\Factory\ContainerFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for commerce_tpay_bank_selection plugins.
 */
abstract class CommerceTpayBankSelectionPluginBase extends PluginBase implements CommerceTpayBankSelectionInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new CommerceTpayBankSelectionPluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *  The logger service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ClientInterface $http_client, LoggerChannelFactoryInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->httpClient = $http_client;
    $this->logger = $logger->get('commerce_tpay');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client'),
      $container->get('logger.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  public function getBanks($payment_gateway_plugin): array {
    $merchant_id = $payment_gateway_plugin->getConfiguration()['merchant_id'];
    $bank_selection_online_only = $payment_gateway_plugin->getConfiguration()['bank_selection_online_only'];
    $banks_url = "https://secure.tpay.com/groups-{$merchant_id}{$bank_selection_online_only}.js?json";
    $banks = [];

    try {
      $response = $this->httpClient->get($banks_url);
      if ($response->getStatusCode() == 200) {
        $banks = json_decode($response->getBody(), TRUE);
      }
      // Optional: Log other non-200 status codes if needed.
      else {
       $this->logger->notice('Unexpected status code received: ' . $response->getStatusCode());
      }
    } catch (\Exception $e) {
      // Log the exception for debugging purposes.
      $this->logger->error('Error fetching banks: ' . $e->getMessage());
    }

    return $banks;
  }


  abstract public function build($payment_gateway_plugin): array;
}
