<?php

namespace Drupal\commerce_tpay;

/**
 * Interface for commerce_tpay_bank_selection plugins.
 */
interface CommerceTpayBankSelectionInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label(): string;

  /**
   * Returns the list of banks.
   *
   * @return array
   *   The list of banks.
   */
  public function getBanks($payment_gateway_plugin): array;

  /**
   * Returns the form element.
   *
   * @return array
   *   The form element.
   */
  public function build($payment_gateway_plugin): array;

}
