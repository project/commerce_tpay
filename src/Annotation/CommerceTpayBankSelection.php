<?php

namespace Drupal\commerce_tpay\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines commerce_tpay_bank_selection annotation object.
 *
 * @Annotation
 */
class CommerceTpayBankSelection extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
