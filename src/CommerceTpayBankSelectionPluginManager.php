<?php

namespace Drupal\commerce_tpay;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * BankSelection plugin manager.
 */
class CommerceTpayBankSelectionPluginManager extends DefaultPluginManager {

  /**
   * Constructs CommerceTpayBankSelectionPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/CommerceTpayBankSelection',
      $namespaces,
      $module_handler,
      'Drupal\commerce_tpay\CommerceTpayBankSelectionInterface',
      'Drupal\commerce_tpay\Annotation\CommerceTpayBankSelection'
    );
    $this->alterInfo('commerce_tpay_bank_selection_info');
    $this->setCacheBackend($cache_backend, 'commerce_tpay_bank_selection_plugins');
  }

}
