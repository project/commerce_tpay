<?php

namespace Drupal\commerce_tpay\PluginForm\TpayRedirect;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * Provides the Off-site payment form.
 */
class TpayPaymentForm extends BasePaymentOffsiteForm implements ContainerInjectionInterface  {

  const TPAY_API_URL = 'https://secure.tpay.com';

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * TpayPaymentForm constructor.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   */
  public function __construct(LanguageManagerInterface $language_manager) {
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $order = $form_state->getFormObject()->getOrder();

    $merchant_id = $payment_gateway_plugin->getConfiguration()['merchant_id'];
    $merchant_secret = $payment_gateway_plugin->getConfiguration()['merchant_secret'];

    $price = $payment->getAmount()->getNumber();
    $currency = $payment->getAmount()->getCurrencyCode();

    $crc = $order->id() . '/' . $currency;

    $current_langcode = strtoupper($this->languageManager->getCurrentLanguage()->getId());
    $tpay_languages = ['DE', 'EN', 'PL'];
    $language = in_array($current_langcode, $tpay_languages) ? $current_langcode : 'EN';

    $parameters = [
      'id' => $merchant_id,
      'amount' => $price,
      'description' => t('Order no') . ' ' . $order->id(),
      'crc' => $crc,
      'md5sum' => md5(implode('&', [$merchant_id, $price, $crc, $merchant_secret])),
      'email' => $order->getEmail(),
      'language' => $language,
      'result_url' => $payment_gateway_plugin->getNotifyUrl(),
      'return_url' => $payment_gateway_plugin->getReturnUrl($order),
      'return_error_url' => $payment_gateway_plugin->getCancelUrl($order),
      ];

    if ($order->getData('tpay_bank_selection')) {
      $parameters['group'] = $order->getData('tpay_bank_selection');
    }

    $parameters['accept_tos'] = 1;

    if ($order->getBillingProfile()) {
      $address = $order->getBillingProfile()->address->first();
      $parameters['name'] = $address->getGivenName() . ' ' . $address->getFamilyName();
      $parameters['address'] = $address->getAddressLine1();
      $parameters['city'] = $address->getLocality();
      $parameters['country'] = $address->getCountryCode();
    }

    return $this->buildRedirectForm($form, $form_state, self::TPAY_API_URL, $parameters, self::REDIRECT_POST);
  }
}
