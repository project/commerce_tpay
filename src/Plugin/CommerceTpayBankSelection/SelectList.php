<?php

namespace Drupal\commerce_tpay\Plugin\CommerceTpayBankSelection;

use Drupal\commerce_tpay\CommerceTpayBankSelectionPluginBase;

/**
 * Plugin implementation of the commerce_tpay_bank_selection.
 *
 * @CommerceTpayBankSelection(
 *   id = "select_list",
 *   label = @Translation("Select list"),
 *   description = @Translation("Banks in select list.")
 * )
 */
class SelectList extends CommerceTpayBankSelectionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function build($payment_gateway_plugin): array {
    $options = [];
    foreach ($this->getBanks($payment_gateway_plugin) as $bank) {
      $options[$bank['id']] = $bank['name'];
    }
    $element = [
      '#type' => 'select',
      '#title' => $this->t('Bank selection'),
      '#description' => $this->t('Select your preferred bank.'),
      '#default_value' => '',
      '#required' => TRUE,
      '#options' => $options,
    ];
    return $element;
  }

}
