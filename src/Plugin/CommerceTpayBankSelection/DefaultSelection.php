<?php

namespace Drupal\commerce_tpay\Plugin\CommerceTpayBankSelection;

use Drupal\commerce_tpay\CommerceTpayBankSelectionPluginBase;

/**
 * Plugin implementation of the commerce_tpay_bank_selection.
 *
 * @CommerceTpayBankSelection(
 *   id = "default_selection",
 *   label = @Translation("Default selection"),
 *   description = @Translation("Banks in default selection.")
 * )
 */
class DefaultSelection extends CommerceTpayBankSelectionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function build($payment_gateway_plugin): array {
    $options = [];
    foreach ($this->getBanks($payment_gateway_plugin) as $bank) {
      $options[$bank['id']] = ['name' => $bank['name'], 'img' => $bank['img']];
    }

    $element = [
      '#theme' => 'commerce_tpay_bank_selection_grid',
      '#title' => $this->t('Bank selection'),
      '#description' => $this->t('Select your preferred bank.'),
      '#default_value' => '',
      '#required' => TRUE,
      '#options' => $options,
    ];

    $element['#attached']['library'][] = 'commerce_tpay/bank_selection';
    return $element;
  }

}
