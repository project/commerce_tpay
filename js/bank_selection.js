(function($, Drupal) {
  Drupal.behaviors.tpay_bank_selection = {
    attach: function(context, settings) {
      $(context).find('.tpay-group-holder').once('tpay_bank_selection').on('click', function() {
        console.log(this);
        const bank_id = this.id.split("-")[1];
        const input = document.getElementById('tpay_bank_selection');
        const bank_block = document.getElementById(`bank-${bank_id}`);
        const active_bank_blocks = Array.from(document.getElementsByClassName('tpay-active'));

        input.value = bank_id;

        active_bank_blocks.forEach(block => block.classList.remove('tpay-active'));

        if (bank_block !== null) {
          bank_block.classList.add('tpay-active');
        }
      });
    }
  };
})(jQuery, Drupal);
